package fr.formation.utils;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class HibernateUtil {
    
    @Getter
    private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("my-persistence-unit");

	private static ThreadLocal<EntityManager> entityManager = new ThreadLocal<>();
	
	public static boolean isEntityManagerClosed() {
		return entityManager.get() == null || !entityManager.get().isOpen();
	}
	
	public static EntityManager getEntityManager() {
		if (entityManager.get() == null || !entityManager.get().isOpen())
			entityManager.set(entityManagerFactory.createEntityManager());
		return entityManager.get();
	}
	
	public static void closeEntityManager() {
		if (entityManager.get() != null || entityManager.get().isOpen()) {
			entityManager.get().close();
			entityManager.remove();
		}	
	}
}
