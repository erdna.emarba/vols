package fr.formation.repositories;

import java.util.Collection;

import fr.formation.models.Vol;


public class VolRepository extends GenericRepository<Vol> {

    public VolRepository() {
        super(Vol.class);
    }

    public Collection<Vol> findByDepartVilleNative(String villeDeDepart) {
        try (var em = entityManagerFactory.createEntityManager()) {
            var q = em.createNativeQuery("SELECT v.id as id, v.num as num, v.arrivee_id as arrivee_id, v.depart_id as depart_id, v.copilote_id as copilote_id, v.pilote_id as pilote_id FROM vol v INNER JOIN aeroport a ON v.depart_id = a.id WHERE a.ville = :ville", Vol.class);
            q.setParameter("ville", villeDeDepart);
            return q.getResultList();
        }
    }

    public Collection<Vol> findByDepartVilleJpql(String villeDeDepart) {
        try (var em = entityManagerFactory.createEntityManager()) {
            var q = em.createQuery("FROM Vol v WHERE v.depart.ville = :ville", Vol.class);
            q.setParameter("ville", villeDeDepart);
            return q.getResultList();
        }
    }

    // rechercher les vols dont le prenom ou le nom du pilote contient une chaine
    public Collection<Vol> findByPiloteNomOrPrenomLike(String query) {
        try (var em = entityManagerFactory.createEntityManager()) {
            var q = em.createQuery("FROM Vol v WHERE v.pilote.nom LIKE :query or v.pilote.prenom LIKE :query", Vol.class);
            q.setParameter("query", "%"+query+"%");
            return q.getResultList();
        }
    }




    
}
