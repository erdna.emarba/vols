package fr.formation.repositories;

import fr.formation.models.Aeroport;

public class AeroportRepository extends GenericRepository<Aeroport> {

    public AeroportRepository() {
        super(Aeroport.class);
    }
}
