package fr.formation.repositories;

import fr.formation.models.Personne;

public class PersonRepository extends GenericRepository<Personne> {

    public PersonRepository() {
        super(Personne.class);
    }
    
}
