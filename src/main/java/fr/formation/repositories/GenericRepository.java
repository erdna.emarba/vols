package fr.formation.repositories;

import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

import fr.formation.utils.HibernateUtil;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;

public class GenericRepository<T> {

    protected EntityManagerFactory entityManagerFactory = HibernateUtil.getEntityManagerFactory();

    private Class<T> clazz;

    public GenericRepository(Class<T> clazz) {
        this.clazz = clazz;
    }

    public static <R> R entityManager(Function<EntityManager, R> function) {
        // return () -> {
            boolean entityManagerMustBeClosed = HibernateUtil.isEntityManagerClosed();
            EntityManager em = HibernateUtil.getEntityManager();
            R result;
            try {
                result = function.apply(em);
            } finally {
                if (entityManagerMustBeClosed)
                    em.close();
            }
            return result;
        // };
    }

    public static <R> Function<EntityManager, R> transaction(BiFunction<EntityManager, EntityTransaction, R> function) {
        return (em) -> {
            EntityTransaction tx = em.getTransaction();
            boolean transactionMustBeManaged = tx != null && !tx.isActive();
            R result;
            try {
                if (transactionMustBeManaged)
                    tx.begin();
                result = function.apply(em, tx);
                if (transactionMustBeManaged)
                    em.getTransaction().commit();
            } catch (Exception e) {
                if (transactionMustBeManaged && em.getTransaction() != null && em.getTransaction().isActive())
                    em.getTransaction().rollback();
                throw e;
            }
            return result;
        };
    }

    public Optional<T> findById(long id) {
        return entityManager(em -> Optional.ofNullable(em.find(clazz, id)));
    }

    public T save(T element) {
        return entityManager(transaction((em, tx) ->  em.merge(element)));
    }

    public void delete(T element) {
        entityManager(transaction((em, tx) ->  {
            var e = em.merge(element);
            em.remove(e);
            return null;
        }));
    }

    public void deleteById(long id) {
        EntityTransaction t = null;
        try (var em = entityManagerFactory.createEntityManager()) {
            t = em.getTransaction();
            t.begin();
            T element = em.getReference(clazz, id);
            em.remove(element);
            t.commit();
        } catch (Exception e) {
            if (t != null)
                t.rollback();
            throw e;
        }
    }

}
