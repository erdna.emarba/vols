package fr.formation;

import fr.formation.models.Aeroport;
import fr.formation.models.Pilote;
import fr.formation.models.Vol;
import fr.formation.repositories.VolRepository;

public class App {

    public static void main(String[] args) {
        var volRepository = new VolRepository();
        volRepository
            .findByPiloteNomOrPrenomLike("walk")
            .forEach(v -> System.out.println("\t- " + v));
        // fixtures();
    }

    private static void fixtures() {
        var volRepository = new VolRepository();

        var marseille = Aeroport.builder().ville("Marseille").pays("France").build();
        var paris = Aeroport.builder().ville("Paris").pays("France").build();
        var newYork = Aeroport.builder().ville("New York").pays("United States").build();
        var tokyo = Aeroport.builder().ville("Tokyo").pays("Japan").build();

        var john = Pilote.builder().nom("smith").prenom("john").build();
        var darth = Pilote.builder().nom("vader").prenom("darth").build();
        var luke = Pilote.builder().nom("skywalker").prenom("luke").build();
        var leila = Pilote.builder().nom("skywalker").prenom("leila").build();

        volRepository.save(Vol.builder()
            .num(1)
            .arrivee(marseille)
            .depart(newYork)
            .pilote(john)
            .copilote(darth)
            .build());
        volRepository.save(Vol.builder()
            .num(2)
            .arrivee(tokyo)
            .depart(paris)
            .pilote(leila)
            .copilote(john)
            .build());
        volRepository.save(Vol.builder()
            .num(3)
            .arrivee(paris)
            .depart(marseille)
            .pilote(darth)
            .copilote(luke)
            .build());
        volRepository.save(Vol.builder()
            .num(4)
            .arrivee(marseille)
            .depart(tokyo)
            .pilote(luke)
            .copilote(leila)
            .build());
        volRepository.save(Vol.builder()
            .num(1)
            .arrivee(newYork)
            .depart(paris)
            .pilote(darth)
            .copilote(leila)
            .build());
    }

}
