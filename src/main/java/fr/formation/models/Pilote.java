package fr.formation.models;

import java.util.Collection;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
@SuperBuilder
@Entity
public class Pilote extends PersonnelVolant {
    
    private String licence;

    @OneToMany(mappedBy = "pilote")
    @ToString.Exclude
    private Collection<Vol> volsPilotes;

    @OneToMany(mappedBy = "copilote")
    @ToString.Exclude
    private Collection<Vol> volsCopilotes;
}
