package fr.formation.models;

import java.util.Collection;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@SuperBuilder
@Entity
public class Vol {

    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue
    private long id;

    private int num;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Aeroport depart;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Aeroport arrivee;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Pilote pilote;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Pilote copilote;

    @ManyToMany
    @JoinTable(name="vol_passager")
    @ToString.Exclude
    private Collection<Passager> passagers;

    @ManyToMany
    @JoinTable(name="vol_steward")
    @ToString.Exclude
    private Collection<Stewards> stewards;
    
}
