package fr.formation.models;

import java.util.Collection;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
@SuperBuilder
@Entity
public class Stewards extends PersonnelVolant {
    
    private String grade;

    @ManyToMany(mappedBy = "stewards")
    private Collection<Vol> vols;
}
