package fr.formation.models;

import java.util.Collection;

import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
@SuperBuilder
@Entity
public class Passager extends Personne {

    @Embedded
    private Passeport passeport;

    @ManyToMany(mappedBy = "passagers")
    private Collection<Vol> vols;

}
