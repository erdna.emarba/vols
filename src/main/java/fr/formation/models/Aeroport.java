package fr.formation.models;

import java.util.Collection;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@SuperBuilder
@Entity
public class Aeroport {

    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue
    private long id;

    private String ville;

    private String pays;

    @OneToMany(mappedBy = "depart")
    @ToString.Exclude
    private Collection<Vol> departs;

    @OneToMany(mappedBy = "arrivee")
    @ToString.Exclude
    private Collection<Vol> arrivees;
}
