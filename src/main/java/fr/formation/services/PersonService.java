package fr.formation.services;

import fr.formation.models.Passager;
import fr.formation.models.Vol;
import fr.formation.repositories.GenericRepository;
import fr.formation.repositories.PersonRepository;
import fr.formation.repositories.VolRepository;

public class PersonService {
    
    private PersonRepository personRepository = new PersonRepository();
    private VolRepository volRepository = new VolRepository();

    void passagerChangerVol(long passagerId, long ancienVolId, long nouveauVolId) {
        GenericRepository.entityManager(GenericRepository.transaction((em, tx) -> {
            Passager passager = (Passager) personRepository.findById(passagerId).get();
            Vol ancienVol = volRepository.findById(ancienVolId).get();
            Vol nouveauVol = volRepository.findById(nouveauVolId).get();
            ancienVol.getPassagers().remove(passager);
            nouveauVol.getPassagers().add(passager);
            return null;
        }));
    }
    void passagerChangerVol2(long passagerId, long ancienVolId, long nouveauVolId) {
        Passager passager = (Passager) personRepository.findById(passagerId).get();
        Vol ancienVol = volRepository.findById(ancienVolId).get();
        Vol nouveauVol = volRepository.findById(nouveauVolId).get();
        ancienVol.getPassagers().remove(passager);
        nouveauVol.getPassagers().add(passager);
        volRepository.save(ancienVol);
        volRepository.save(nouveauVol);
    }
}
